async function main () {
    window.pyodide = await loadPyodide();
    await pyodide.loadPackage(['opencv-python', 'matplotlib'])
    await writeCustomFile('test.xml', 'test.xml')
}

function changeInput (event) {
    var reader = new FileReader();
    reader.onload = function () {
        writeBase64ToFile(reader.result)
    };
    reader.readAsDataURL(event.target.files[0]);
};

function writeBase64ToFile (base64String) {
    document.getElementById("representImg").src = base64String
    var formatedBase64String = base64String.split('base64,')[1]
    pyodide.runPython(`
    import js
    import base64
    base64_string = "${formatedBase64String}"
    base64_string_decoded = base64.b64decode(base64_string)

    image_result = open('test.jpg', 'wb')
    image_result.write(base64_string_decoded)
    `)
}

async function analizeImage () {
    await callOpenCV()
    extractImage()
}

// function extractFile () {
//     pyodide.runPython(`
//         from js import Blob, document
//         from js import window
          
//         with open('test.xml', 'rt') as fh:
//           txt = fh.read()
            
//         #blob = Blob.new([txt], {type : 'application/text'})
//         #url = window.URL.createObjectURL(blob) 
//         #window.open(url, '_blank');
//         `)
// }

function extractImage () {
    pyodide.runPython(`
        import base64

        with open("test.jpg", "rb") as image_file:
          encoded_string= base64.b64encode(image_file.read())
        outputImage = encoded_string.decode('utf-8')
        `)

    document.getElementById("representImg").src = 'data:image/jpg;base64,' + pyodide.globals.get('outputImage')
}

async function writeCustomFile (name, url) {
    await pyodide.runPythonAsync(`
    from pyodide.http import pyfetch
    response = await pyfetch("${url}")
    if response.status == 200:
      with open("${name}", "wb") as f:
        f.write(await response.bytes())
    `)
}

async function callOpenCV () {
    await pyodide.runPythonAsync(`
    import cv2

    face_cascade = cv2.CascadeClassifier('test.xml')
    
    img = cv2.imread('test.jpg')
    
    gray = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)
    
    faces = face_cascade.detectMultiScale(gray, 1.1, 4)
    print(faces)
    for (x, y, w, h) in faces:
        cv2.rectangle(img, (x, y), (x+w, y+h), (255, 0, 0), 2)

    cv2.imwrite('test.jpg', img)
    `)
}

main();